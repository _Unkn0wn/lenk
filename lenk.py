import Lenk, sys

try:
    if "-v" in sys.argv or "-verbose" in sys.argv:
        print("Running Lenk script: " + sys.argv[1])
    print(Lenk.Run("runscript:" + sys.argv[1]))
except IndexError:
    print("No file given.")