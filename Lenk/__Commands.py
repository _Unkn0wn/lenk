# from . import Extensions
global Controller, Error, Run
from .__Controller import Controller, Error, Run

class Commands():
    #* text:Hello, world!

    
    def Text(self, args, arg):
        return arg.replace('//', ' ').replace('/', '\n')

    #* numeric:23*3


    def Math(self, args, arg):
        import subprocess, os
        p = subprocess.run([f"{os.path.dirname(os.path.abspath(__file__))}{os.path.sep}Math{os.path.sep}Math.exe"], shell=False, capture_output=True, input=arg.encode())
        return p.stdout.decode()

    #* openfile:File.txt


    def Readfile(self, args, arg):
        ReturnString = r''
        FileStream = None
        try:
            FileStream = open(arg, 'r')
        except IOError as e:
            ReturnString = str(Error(
                'File Error', f'The file "{arg}" could not be opened. {e}'))
        except UnicodeError:
            ReturnString = str(Error(
                'File Error', f'The file "{arg}" could not be read.'))

        try:
            if FileStream:
                ReturnString += FileStream.read()
        except IOError:
            ReturnString += str(Error(
                'File Error', f'The file "{arg}" could not be read.'))

        return ReturnString

    #* set:Lenk/0.1.3


    def Set(self, args, arg):
        ReturnString = ''
        try:
            Controller.Set(args[0], args[1])
        except IndexError:
            ReturnString = str(Error('Argument Error',
                                    'No variable name/value supplied'))

        return ReturnString

    #* get:Lenk


    def Get(self, args, arg):
        ReturnString = ''
        try:
            ReturnString = Controller.Get(args[0])
        except IndexError:
            ReturnString = str(Error('Argument Error',
                                    'No variable name supplied'))

        return ReturnString

    #* version


    def Version(self, args, arg):
        ReturnString = ''
        if "only" in arg.lower():
            ReturnString = Controller.Version
        else:
            ReturnString = f'Lenk Version {Controller.Version}'

        return ReturnString

    #* help


    def Help(self, args, arg):
        ReturnString = Controller.Help()

        return ReturnString

    #* clear/cls


    def Clear(self, args, arg):
        ReturnString = ''
        print("\033[2J" + '\n'*(500))

        return ReturnString


    def Cls(self, args, arg):
        return self.Clear(args, arg)

    #* quit/exit


    def Exit(self, args, arg):
        exit(0 if not arg else arg)
        return None


    def Quit(self, args, arg):
        self.Exit(args, arg)
        return None


    #* runscript:scriptfile
    def Runscript(self, args, arg):
        if args and arg:
            script = self._GetFileString(args[0])
            if not script:
                return str(Error("IO Error", "File doesnt exist or is empty. " + script))
            else:
                return Run(script, True)
            return ""
        else:
            return Error("Argument Error", "No file was supplied.")

    def _FileExists(self, _file):
        try:
            open(_file).close()
        except IOError:
            return False
        return True

    def _GetFileString(self, _file):
        if (self._FileExists(_file)):
            with open(_file, "r") as f:
                return f.read()
        else:
            return ""

    def _GetFileBytes(self, _file):
        if (self._FileExists(_file)):
            with open(_file, "rb") as f:
                return f.read()
        else:
            return ""

    #todo EXTENSIONS
    #* import:extFile/extName
    def Import(self, args, arg):
        return "Not implemented."
        #// if args and arg:
        #//     fcontent = self._GetFileString(args[0])
        #//     if fcontent:
        #//         setattr(self, args[1], eval(fcontent))
        #//         A = getattr(self, args[1])
        #//         return A()
        #//     else:
        #//         return Error("IO Error", f'The file "{args[0]}" could not be opened.')


        #// try:
        # //    Command = args[0]
        #// except IndexError:
        #//     Command = None
        #//     ReturnString = str(Error('Arguments Error', 'No Extension supplied'))

        #// try:
        #//     Arguments = args[1:]
        #//     Argument = arg.replace(f'{Command}/', '')
        #// except IndexError:
        #//     Arguments = []
        #//     Argument = ''
