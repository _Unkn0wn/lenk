# Main controller

class _Controller:
    def __init__(self):
        # help
        self.HelpText = [
            [
                'Help',
                'Responds with this',
                'None'
            ],
            [
                'Version',
                'Responds with the current version',
                '[only]'
            ],
            [
                'Text',
                'Responds with input',
                '{String}'
            ],
            [
                'Math',
                'Evaluates mathmatical expressions',
                '{Expression}'
            ],
            [
                'Clear/Cls',
                'Clears the terminal',
                'None'
            ],
            [
                'Quit/Exit',
                'Exits the terminal',
                '[String OR Number]'
            ],
            [
                'Set',
                'Sets a variable',
                '{Name} & {Value}'
            ],
            [
                'Get',
                'Gets a variable',
                '{Name}'
            ],
            [
                'ReadFile',
                'Gets the contents of a file',
                '{Path}'
            ],
            [
                'RunScript',
                'Runs the contents of a file as a script',
                '{Path}'
            ]
        ]
        # version
        self.Version = "0.1.5"
        #set and get
        self.__Variables = {}

    # set
    def Set(self, Name, Value):
        if not Name:
            return Error('Argument Error', 'No variable name is supplied')

        self.__Variables.update({Name: Value})
        return True
    # get

    def Get(self, Name):
        if not Name:
            return Error('Argument Error', 'No variable name is supplied')

        if not Name in self.__Variables:
            return Error('Variable Error', f'The variable "{Name}" does not exist')

        return self.__Variables[Name]
    # help

    def Help(self):
        HelpString = ''
        count = 0

        for helpcmd in self.HelpText:
            count += 1
            HelpString += f'{helpcmd[0]}: {helpcmd[1]}\nargs: {helpcmd[2]}\n' if count == 1 else f'\n{helpcmd[0]}: {helpcmd[1]}\nargs: {helpcmd[2]}\n'

        return HelpString

class Env:
    def __init__(self):
        self.__environment = {}

    def __get__(self, instance, key=0):
        return self.__environment[key]

    def __set__(self, instance, value: dict):
        self.__environment.update(value)

global Environment, Controller

Controller = _Controller()

Environment = Env()
    
global sub, Error, Parse, Run
try:
    from re import sub
except ImportError:
    sub = None

from .__Error import Error
from .__Parse import Parse


def Run(Input: str, script: bool = False):
    Output = ''
    if not type(Input) is str:
        raise SyntaxError("Run(String)")

    # parse input into commands
    cmds = Parse(Input)

    # run every command
    for cmd in cmds:
        # if the command isnt just ""
        if cmd[0]:
            # for if the command doesnt exist
            try:
                _cmds = Commands()
                # if the command is running from a script (see runscript command)
                if script:
                    # for if Script_{command} doesnt exist
                    try:
                        if cmd[0] == "Exit" or cmd[0] == "Quit":
                            return Output
                        Function = getattr(_cmds, f"Script_{cmd[0]}")
                    except AttributeError:
                        Function = getattr(_cmds, cmd[0])
                else:
                    Function = getattr(_cmds, cmd[0])
                
                funcOut = str(Function(cmd[1], cmd[2]))

                # if arg pipes are available
                if cmd[3]:
                    # get output of pipe function
                    # _funcOut = ""
                    # run every pipe
                    # print(funcOut)
                    for pipe in cmd[3]:
                        # get command and arguments of the pipe
                        pipe = Parse(pipe)[0]
                        
                        # make default arg the output of the original command
                        arg = funcOut

                        # if the pip has arguments then add the output to the arguments

                        if pipe[1] and pipe[2]:
                            arg = f"{pipe[2]}{funcOut}"

                        # run pipe and get output
                        funcOut = Run(f"{pipe[0]}: {arg}", script)


                Output += funcOut
            # if the command doesnt exist getattr() raises an AttributeError
            except AttributeError:
                Output += str(Error('Command Not Found',
                                    f'The command "{cmd[0]}" does not exist'))

    # Give all commands output to caller
    return Output

global Commands

from .__Commands import Commands
