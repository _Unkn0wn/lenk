global Error
from .__Controller import Error,sub

# Parse(Input)

def Parse(Input: str):
    Output = ''

    # For scripting
    LineSplit = Input.split('\n')
    ComSplit = []

    for Split in LineSplit:
        ComSplit.append(Split.split(';'))

    Parsed = []

    #todo Pipes = set: Text/Hey; get: Text :: text
    #* ^ outputs: Hey

    for nestcmd in ComSplit:
        for cmd in nestcmd:
            # Separate Command and Arguments, CMD ARG/ARGS
            if sub:
                cmd = sub(r'[/]', '/', sub(r'[ ]', ' ', sub(r'[;]', ';', cmd))).replace('/ ', '/').replace(' /','/')
            Separated = cmd.split(':', 1)

            _cmd = ''
            _args = []

            try:
                Separated[1] = sub(r"[ ]*:::[ ]*", ':::', Separated[1])

                _pipes = []
                _findpipe = Separated[1].find(":::")

                if ":::" in Separated[1]:
                    _pipes = Separated[1].split(":::")[1:]

                if _findpipe > 0:
                    Separated[1] = Separated[1][:_findpipe]
                    
            except IndexError:
                _pipes = []

            # extract full argument, ARG/ARGS
            try:
                # if regex is available make scripting easier
                if sub:
                    _arg = sub(r'^[ ]*', '', Separated[1])
                else:
                    _arg = Separated[1]
            except IndexError:
                _arg = ''

            # extract command, CMD
            try:
                _cmd = Separated[0].lower().replace(' ', '')
            except IndexError:
                _cmd = None
                Output += str(Error('Parser Error', f'No command given'))

            # extract separate arguments, ARG ARGS
            try:
                if sub:
                    _args = sub(r'^[ ]*', '', Separated[1]).split('/')
                else:
                    _args = Separated[1].split('/')
            except IndexError:
                _args = []


            # make first character of command capital
            _cmd = f'{_cmd[0].upper()}{_cmd[1:]}' if len(_cmd) > 2 else _cmd

            # Add parsed command to Parsed
            Parsed.append([_cmd, _args, _arg, _pipes])

    # Give Caller parsed input
    return Parsed
