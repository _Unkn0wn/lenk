# Lenk

Lenk is a programming language I like to work on from time to time.
<br/>Lenk is currently in it's 15th version, <i>Lenk 0.1.5</i>.

To run Lenk you need Python 3(.8.2).

You can use LenkConsole.py to run commands or you can use your own code<br/>
Like this:
```python
import Lenk.Run as Run
print(Run("text: Hello, World!"))
```